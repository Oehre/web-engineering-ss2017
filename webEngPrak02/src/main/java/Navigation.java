import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by Öhre on 05.05.2017.
 */
@WebServlet(urlPatterns = {"/Navigation"})
public class Navigation extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //get PrintWriter for output
        PrintWriter out = response.getWriter();
        out.append("<body>");
        //Nav section
        out.append("<nav>");
        //begin list
        out.append("<ul>");
        //list-entry with link to homepage
        out.append("<li> <a href=\""+response.encodeURL("?action=home")+"\">Startseite <a>");
        //list-entry with link to headerpage
        out.append("<li> <a href=\""+response.encodeURL("?action=header")+"\">Header <a>");
        //list-entry with link to cookiepage
        out.append("<li> <a href=\""+response.encodeURL("?action=cookies")+"\">Kekse <a>");
        //list-entry with link to google
        out.append("<li> <a href=\""+response.encodeURL("?action=search")+"\">Google <a>");
        out.append("</ul>");
        out.append("</nav>");
        out.append("</body>");
    }
}
