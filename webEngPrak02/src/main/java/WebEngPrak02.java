import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Enumeration;

/**
 * Servlet implementation class HelloWorldServlet
 */
@WebServlet(urlPatterns = {"/WebEngPrak02"})
public class WebEngPrak02 extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public WebEngPrak02() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //init action string
        String action = "";
        //get PrintWriter for output
        PrintWriter out = response.getWriter();
        //Page Title
        out.append("<body><h1>2. Praktikum WebEng</h1> </br>");

        //Check whether a action is set
        if (request.getParameter("action") != null) {
            //save value of parameter "action"
            action = request.getParameter("action");
        }

        //get Dispatcher to include nav-list
        RequestDispatcher dispatcher = request.getRequestDispatcher("/Navigation");
        //include nav-list
        dispatcher.include(request, response);

        //check value of action
        switch (action) {
            //homepage and defaultpage
            case "home":
            default:
                //add homepage infos
                out.append("<h2>Startseite</h2>" +
                        "Dies ist die Startseite - yeah- </br> " +
                        "Jeremy Gondolf </br>" +
                        "701721");
                break;
            //page displaying all headers in the request
            case "header":
                //title
                out.append("<h2>HttpHeader</h2> </br>");
                //begin table
                out.append("<table style = \"width\">");
                //table head row
                out.append("<tr>");
                out.append("<th> Name </th> <th> Wert </th> <th> Beschreibung </th> ");
                out.append("</tr>");
                //get and  iterate over all headerNames (Enumeratioin)
                for (Enumeration<String> headerNames = request.getHeaderNames(); headerNames.hasMoreElements(); ) {
                    //save current headerName
                    String headerName = headerNames.nextElement();
                    out.append("<tr>");
                    out.append("<td>" + headerName + "  </td>");
                    out.append("<td>");
                    //get value of Header-Entry with current name
                    out.append(request.getHeader(headerName));
                    out.append("</td>");
                    out.append("<td> </td>");
                    out.append("</tr>");
                }
                out.append("</table>");
                break;
            //page displaying all currently saved cookies
            case "cookies":
                //get array of currently saved cookies
                Cookie[] cookies = request.getCookies();
                out.append("<h2>Cookies</h2> </br>");
                //display cookies in table
                out.append("<table>");
                out.append("<tr>");
                out.append("<th> Name </th> <th> MaxAge </th> ");
                out.append("</tr>");
                //check if at least one cookie exists
                if (cookies != null) {
                    //iterate over all cookies
                    for (Cookie c : cookies) {
                        //print cookie name and MaxAge
                        out.append("<tr> \n"
                                + "<td>" + c.getName() + " </td> \n"
                                + "<td>" + c.getMaxAge() + "</td>\n"
                                + "</tr>");
                    }
                }
                out.append("</table>");
                //get current time
                LocalDateTime localDateTime = LocalDateTime.now();
                //create new Cookie with current time formated in the name
                Cookie custumCookie = new Cookie("keks" + DateTimeFormatter.ofPattern("yyyMMddHHmmss").format(localDateTime), "");
                //set MaxAge of the cookie
                custumCookie.setMaxAge(30);
                //add cookie to response
                response.addCookie(custumCookie);
                break;
            case "search":
                //construct URL for redireciton
                String searchEngine = response.encodeURL("http://www.google.de");
                //redirect
                response.sendRedirect(searchEngine);
                break;

        }
        out.append("</bodyY");
        response.setContentType("text/html");
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
        doGet(request, response);
    }

}
