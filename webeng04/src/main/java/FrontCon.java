import webeng.User;
import webeng.businesslogic.UserManager;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.websocket.Session;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet(urlPatterns = {"/"})
public class FrontCon extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("Inside frontcon");
        processRequest(request, response);
    }

    public void processRequest (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher rd;
        List<String> flash;
        String action = request.getParameter("action");
        if(action == null) {
            action = "";
        }
        String redir = "";

        switch (action){
            case "management":
                redir = "/management";
                break;
            case "detail":
                if(request.getParameter("userID")!= null) {
                    User nUser = new User();
                    nUser.setID(Integer.parseInt(request.getParameter("userID")));
                    UserManager userManager = new UserManager();
                    if(request.getParameter("delete")!= null) {
                        if(userManager.deleteUser(nUser)){
                            request.getSession().setAttribute("deletUser",true);
                            redir = "/management";
                            break;
                        } else {
                            request.getSession().setAttribute("deletUser",false);
                        }
                    }
                    User tUser = userManager.getUserById(nUser);
                    request.getSession(true).setAttribute("tUser", tUser);
                    if(request.getParameter("submit")!= null){
                        tUser.setName(request.getParameter("userName"));
                        tUser.setEmail(request.getParameter("userEmail"));
                        if(userManager.updateUser(tUser)){
                            request.getSession().setAttribute("updateUser",true);
                            redir="/management";
                            break;
                        } else {
                            request.getSession().setAttribute("updateUser",false);
                        }
                    }
                    redir ="/userManager/presentation.jsp";
                }
                break;
            case"index":
                redir = "/index.jsp";
                break;
            case "login":
                flash = new ArrayList<>();
                if (request.getParameter("login")!=null){
                    HttpSession userSession = request.getSession(true);
                    String email = request.getParameter("userEmail");
                    String password = request.getParameter("userPassword");
                    if (email==null|| email.equals("")) {
                       flash.add("Missing Email!");
                    }
                    if (password ==null ||password.equals("")){
                        flash.add("Missing Password!");
                    }
                    if (email!=null && password != null && !email.equals("") && !password.equals("")){
                        UserManager userManager = new UserManager();
                        User user = new User();
                        user.setEmail(email);
                        user = userManager.getUserByEmail(user);
                        if (!user.getEmail().equals("")){
                            if (user.getPassword().equals(password)){
                                user.setSessionId(userSession.getId());
                                userSession.setAttribute("user", user);
                                userManager.updateUser(user);
                                flash.add("Login successful!");
                            }
                            else {
                                flash.add("Wrong Password");
                            }
                        }
                        else {
                            flash.add("No user registered with this E-Mail.");
                        }

                    }
                    userSession.setAttribute("flash", flash);
                }
                if (request.getParameter("logout")!= null){
                    HttpSession session = request.getSession();
                    session.invalidate();
                }
                redir = "/userManager/login.jsp";
                break;
            default: case"":case "home":
                redir = "/userManager/login.jsp";
                break;
        }
        rd = request.getRequestDispatcher(redir);
        rd.forward(request,response);

    }
}
