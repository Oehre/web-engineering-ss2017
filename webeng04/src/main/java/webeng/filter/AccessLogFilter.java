package webeng.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
@WebFilter(
        urlPatterns = "/*",
        filterName = "AccessLogFilter",
        servletNames = "FrontCon"
)
public class AccessLogFilter implements Filter {
    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) resp;
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        String timestamp = sdf.format(new Date());

        System.out.println("Request Zugriff auf:" + request.getRequestURI() +" Query:"+ request.getQueryString() + " um:" + timestamp + " Statuscode: " + response.getStatus());
        chain.doFilter(req, resp);
        System.out.println("Response Zugriff auf:" + request.getRequestURI() +" Query:"+ request.getQueryString() + " um:" + timestamp + " Statuscode: " + response.getStatus());
    }

    public void init(FilterConfig config) throws ServletException {

    }

}
