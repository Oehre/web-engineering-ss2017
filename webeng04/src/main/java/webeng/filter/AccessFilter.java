package webeng.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter(
        value = "/*",
        filterName = "AccessFilter",
        servletNames = "FrontCon"
)
public class AccessFilter implements Filter {
    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        System.out.println("in accessfilter");
        HttpServletRequest request = (HttpServletRequest) req;
        HttpSession session = request.getSession();
        if (request.getParameter("action")!=null && !request.getParameter("action").equals("") &&request.getParameter("action").equals("management")) {
            if (session.getAttribute("user") == null) {
                HttpServletResponse response = (HttpServletResponse) resp;
                response.sendError(HttpServletResponse.SC_FORBIDDEN, "Zugriff ist nichterlaubt");
                return;
            } else {
                // Weiterleitung
                chain.doFilter(req, resp);
            }
        }
        chain.doFilter(req,resp);


    }

    public void init(FilterConfig config) throws ServletException {

    }

}
