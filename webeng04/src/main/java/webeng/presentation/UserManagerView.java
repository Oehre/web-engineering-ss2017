package webeng.presentation;

import webeng.User;
import webeng.businesslogic.*;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

@WebServlet(urlPatterns = {"management"})
public class UserManagerView extends HttpServlet {
    private static ArrayList<User> userList = new ArrayList<>();
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if(request.getParameter("submit") != null) {
            String userName = request.getParameter("userName");
            String userEmail = request.getParameter("userEmail");
            String userPassword = request.getParameter("userPassword");
            if ((userName != null && !userName.equals(""))
                    && (userEmail != null && !userEmail.equals(""))
                    && (userPassword != null && !userPassword.equals(""))) {

                HttpSession session = request.getSession(true);
                UserManager manager = new UserManager();

                User nUser = new User();
                nUser.setName(userName);
                nUser.setEmail(userEmail);
                nUser.setPassword(userPassword);
                nUser.setSessionId(session.getId());
                if(manager.addUser(nUser)){
                    response.sendRedirect("/webeng04/?action=management&message=success");
                } else {
                    response.sendRedirect("/webeng04/?action=management&message=email");
                }
            } else {
                response.sendRedirect("/webeng04/?action=management&message=error");
            }
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("/userManager/nav.jsp");
        dispatcher.include(request,response);
        UserManager manager = new UserManager();
        userList = manager.getAllUsers();
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        if (request.getSession().getAttribute("user")!=null){
            User user = (User)request.getSession().getAttribute("user");
            out.println("Logged in as: "+ user.getName());
        }
        //out.println("<textarea readonly name='message' rows='25' cols='50'>");
        int count = 1;
        out.println("<table>");
        for (User u : userList) {
            out.println("<tr>"+u.toString()+"<td> <a href='?action=detail&userID="+u.getID()+"'>Edit</a><td><tr>");
        }
        out.println("</table>");
        //out.println("</textarea>");
        out.println("<form action='/webeng04/?action=management' method='POST'>");
        out.println("Name: <input type='TEXT' name='userName'/>");
        out.println("Email: <input type='Email' name='userEmail'/>");
        out.println("Passwort: <input type='Password' name='userPassword'/>");
        out.println("<button type='submit' name='submit' value='submit'>GO</button>");
        if(request.getParameter("message")!= null && request.getParameter("message").equals("error")) {
            out.println("Bitte fuellen sie die felder aus!!");
            out.println("<br>");
        }
        if(request.getParameter("message")!= null && request.getParameter("message").equals("email")) {
            out.println("Ein Benutzer mit dieser Email ist bereits vorhanden!!");
            out.println("<br>");
        }
        if(request.getParameter("message")!= null && request.getParameter("message").equals("success")) {
            out.println("Benutzer wurde erfolgreich hinzugefuegt.");
            out.println("<br>");
        }
    }
}
