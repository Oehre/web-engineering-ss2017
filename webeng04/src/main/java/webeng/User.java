package webeng;

import java.io.Serializable;

public class User implements Serializable{
    private int ID;
    private String name;
    private String email;
    private String password;
    private String sessionId;

    public User() {

    }

    @Override
    public String toString() {
        return "<td>ID:"+getID()+"</td><td> Name:"+getName()+"</td><td> Email:"+getEmail()+"</td><td> SessionID:"+getSessionId()+"</td>";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }
}
