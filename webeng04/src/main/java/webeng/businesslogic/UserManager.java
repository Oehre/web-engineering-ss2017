package webeng.businesslogic;

import webeng.User;
import webeng.data.DAOFactory;
import webeng.data.UserDAO;

import java.util.ArrayList;

public class UserManager {

    private UserDAO userDAO;

    public UserManager () {
        this.userDAO =  DAOFactory.getDAOFactory(DAOFactory.Backend.H2).getUserDAO();
    }

    public boolean addUser (User u) {
        if(this.userDAO.getUserByEmail(u).getEmail().equals("")) {
            this.userDAO.addUser(u);
            return true;
        } else {
            System.out.println("Email schon  vorhanden");
           return false;
        }
    }

    public boolean deleteUser (User u){
        return this.userDAO.deleteUser(u);
    }

    public boolean updateUser(User u) {
        return this.userDAO.updateUser(u);
    }

    public User getUserByEmail (User u) {
        return this.userDAO.getUserByEmail(u);
    }

    public User getUserById (User u) {
        return this.userDAO.getUserById(u);
    }

    public ArrayList<User> getAllUsers () {
        return this.userDAO.getAllUsers();
    }
}
