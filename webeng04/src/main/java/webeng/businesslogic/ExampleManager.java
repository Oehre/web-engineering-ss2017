package webeng.businesslogic;

import java.util.Collection;

import webeng.data.DAOFactory;
import webeng.data.DAOFactory.Backend;
import webeng.data.ExampleDAO;


public class ExampleManager {
	ExampleDAO exampleDAO;
	
	public ExampleManager() {
		super();
		exampleDAO = DAOFactory.getDAOFactory(Backend.H2).getExampleDAO();
	}
	
	// add methods for managing data 
}
