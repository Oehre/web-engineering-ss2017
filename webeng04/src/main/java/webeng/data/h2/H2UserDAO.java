package webeng.data.h2;

import webeng.User;
import webeng.data.UserDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class H2UserDAO implements UserDAO {

    private Connection con;

    public H2UserDAO () {
        this.con = H2DAOFactory.getConnection();
    }



    @Override
    public boolean addUser(User newUser) {
        String sql = "INSERT INTO USERS (NAME, EMAIL, PASSWORD, SESSIONID) Values (?, ?, ?, ?)";
        PreparedStatement stmt = null;
        boolean success = false;
        try {
            stmt = this.con.prepareStatement(sql);
            stmt.setString(1,newUser.getName());
            stmt.setString(2,newUser.getEmail());
            stmt.setString(3,newUser.getPassword());
            stmt.setString(4,newUser.getSessionId());
            success = stmt.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return success;
    }

    @Override
    public boolean deleteUser(User delUser) {
        String sql = "DELETE FROM USERS Where ID =?";
        PreparedStatement stmt = null;
        boolean success = false;
        try {
            stmt = this.con.prepareStatement(sql);
            stmt.setInt(1,delUser.getID());
            stmt.execute();
            success = true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return success;
    }

    @Override
    public boolean updateUser(User delUser) {
        String sql = "Update USERS Set NAME=?, EMAIL=?, PASSWORD=?, SESSIONID=? Where ID=?";
        PreparedStatement stmt = null;
        boolean success = false;
        try {
            stmt = this.con.prepareStatement(sql);
            stmt.setString(1,delUser.getName());
            stmt.setString(2,delUser.getEmail());
            stmt.setString(3,delUser.getPassword());
            stmt.setString(4,delUser.getSessionId());
            stmt.setInt(5,delUser.getID());
            return (stmt.executeUpdate()==1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return success;
    }

    @Override
    public User getUserByEmail(User customerUser) {
        String sql = "SELECT * FROM USERS Where EMAIL = ?";
        User rUser = new User();
        try {
            PreparedStatement stmt = this.con.prepareStatement(sql);
            stmt.setString(1, customerUser.getEmail());
            ResultSet set = stmt.executeQuery();
            if(!set.next()) {
                rUser.setEmail("");
                return rUser;
            } else {
                rUser.setID(set.getInt("ID"));
                rUser.setName(set.getString("NAME"));
                rUser.setEmail(set.getString("EMAIL"));
                rUser.setPassword(set.getString("PASSWORD"));
                rUser.setSessionId(set.getString("SESSIONID"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rUser;
    }

    @Override
    public User getUserById(User customerUser) {
        String sql = "SELECT * FROM USERS Where ID = ?";
        User rUser = new User();
        try {
            PreparedStatement stmt = this.con.prepareStatement(sql);
            stmt.setInt(1, customerUser.getID());
            ResultSet set = stmt.executeQuery();
            set.next();
            rUser.setID(set.getInt("ID"));
            rUser.setName(set.getString("NAME"));
            rUser.setEmail(set.getString("EMAIL"));
            rUser.setPassword(set.getString("PASSWORD"));
            rUser.setSessionId(set.getString("SESSIONID"));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rUser;
    }


    @Override
    public ArrayList<User> getAllUsers() {
        String sql = "SELECT * FROM USERS";
        ArrayList<User> userList = new ArrayList<>();
        try {
            PreparedStatement stmt = this.con.prepareStatement(sql);
            ResultSet set = stmt.executeQuery();
            while(set.next()) {
                User rUser = new User();
                rUser.setID(set.getInt("ID"));
                rUser.setName(set.getString("NAME"));
                rUser.setEmail(set.getString("EMAIL"));
                rUser.setPassword(set.getString("PASSWORD"));
                rUser.setSessionId(set.getString("SESSIONID"));
                userList.add(rUser);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return userList;
    }
}
