package webeng.data;

import webeng.User;

import java.util.ArrayList;

public interface UserDAO {
    public boolean addUser(User newUser);
    public boolean deleteUser(User delUser);
    public boolean updateUser(User delUser);
    public User getUserByEmail(User customerUser);
    public User getUserById(User customerUser);
    public ArrayList<User> getAllUsers();
}
