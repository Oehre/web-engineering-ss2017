<%--
  Created by IntelliJ IDEA.
  User: Lord Oehre
  Date: 08.06.2017
  Time: 14:15
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ page import="webeng.User" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Login</title>
</head>
<body>
<% RequestDispatcher dispatcher = request.getRequestDispatcher("/userManager/nav.jsp");
    dispatcher.include(request,response);
    %>
<c:if test="${sessionScope.user == null}">
    <h2>Login</h2>
    <form name="login" action="/webeng04/?action=login" method="post">
        E-Mail: <input type="email" name="userEmail"/> <br>
        Password: <input type="password" name="userPassword" /> <br>
        <button type="submit" name="login" value="login">Login</button>
    </form>
</c:if>
<c:if test="${sessionScope.user != null}">
    Hallo, <c:out value="${sessionScope.user.getName()}"/>!<br>
    <form name="Logout" method="post" action="/webeng04/?action=login">
        <button type="submit" name="logout" value="logout">Logout</button>
    </form>
</c:if>
<c:forEach items="${sessionScope.flash}" var="item">
    ${item}<br>
</c:forEach>
</body>
</html>
