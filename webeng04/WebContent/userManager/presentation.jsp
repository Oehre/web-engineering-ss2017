<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>UserVerwaltung</title>
</head>
<body>
<% RequestDispatcher dispatcher = request.getRequestDispatcher("/userManager/nav.jsp");
    dispatcher.include(request,response);
%>
<jsp:useBean id="tUser" class="webeng.User" scope="session"/>
<%
if(tUser.getID()== 0){
    out.println("Ungueltige, bitte eine richtige ID übergeben!");
} else {
    out.println("Daten erfolgreich gelanden!");
}
if(request.getSession().getAttribute("deletUser")!= null) {
    if ((boolean) request.getSession().getAttribute("deletUser")) {
        out.println("User deleted");
    } else {
        out.println("Error deletion failed");
    }
    request.getSession().removeAttribute("deletUser");
}
if(request.getSession().getAttribute("updateUser")!= null) {
    if((boolean) request.getSession().getAttribute("updateUser")) {
        out.println("User updated");
    } else {
        out.println("Error update failed");
    }
    request.getSession().removeAttribute("updateUser");
}

%>

<form name="userData" method="POST">
    Name: <input type='TEXT' name='userName' value='<jsp:getProperty name='tUser' property='name'/>' required/>
    Email: <input type='Email' name='userEmail' value='<jsp:getProperty name='tUser' property='email'/>' />
    Password: <input type='password' name='userPassword' value='<jsp:getProperty name='tUser' property='password'/>' />
    <button type='submit' name='submit' value='submit'>Submit</button>
    <button type='submit' name='delete' value='delete'>Delete</button>
</form>
</body>
</html>
