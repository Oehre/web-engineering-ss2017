<%--
  Created by IntelliJ IDEA.
  User: Lord Oehre
  Date: 08.06.2017
  Time: 15:46
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<ul>
    <li><a href="?action=login">Login</a></li>
    <li><a href="?action=index">Index</a></li>
    <c:if test="${sessionScope.user != null}">
        <li><a href="?action=management">Usermanagement</a> </li>
    </c:if>
</ul>