<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>JSPDateTest</title>
</head>
<body>
<% RequestDispatcher dispatcher = request.getRequestDispatcher("/userManager/nav.jsp");
    dispatcher.include(request,response);
%>
    <%!
        int erg2 = 3+5;
    %>
    <%
        System.out.println("inside JSP");
        int erg =(int) Math.pow(5,2);
    %>
    <p>Aktuelle Datum:
        <%=new java.text.SimpleDateFormat("dd-MM-yyyy").format(new java.util.Date())%>
        <br>
        5^2=
        <%=erg%><br>
        3+5=
        <%=erg2%>

    </p>
    <p>
        Passwort Generator:<br>
        <%
            HttpServletRequest r = (HttpServletRequest) request;
            String codeValue = "";
            String shifted = "";
            if(r.getParameter("codeValue") == null
                    || r.getParameter("codeValue").equals("")
                    || r.getParameter("shiftValue") == null
                    || r.getParameter("shiftValue").equals("")) {
                out.println("Ungueltige Eingabe, bitte füllen sie die Felder aus!");
            } else {
                codeValue = r.getParameter("codeValue");
                int shift = Integer.parseInt(r.getParameter("shiftValue"));
                char[] chars = codeValue.toCharArray();
                for(int i = 0; i < chars.length; i++) {
                    chars[i] -= 'a';
                    chars[i] += shift;
                    chars[i] = (char) (chars[i]% 26);
                    chars[i] += 'a';
                }
                shifted = new String(chars);
            }

        %>
        <form name="generator" method="POST">
            Codewort: <input type='TEXT' name='codeValue' required/>
            Verschiebung: <input type='TEXT' name='shiftValue' required/>
            <button type='submit' name='submit' value='submit'>Submit</button>
        </form>
        Codewort:<%= codeValue%>
        Ergebnis:<%= shifted%>
    </p>
    <p>
        HTTP-Method:<%= r.getMethod()%><br>
        Param:<%= r.getServletContext().getInitParameter("Keks")%>
    </p>
</body>
</html>
