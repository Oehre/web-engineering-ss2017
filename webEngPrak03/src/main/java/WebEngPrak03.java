import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Enumeration;

/**
 * Servlet implementation class HelloWorldServlet
 */
@WebServlet(urlPatterns = {"/WebEngPrak03"})
public class WebEngPrak03 extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public WebEngPrak03() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = "";
        PrintWriter out = response.getWriter();
        if (request.getQueryString() != null) {
            action = request.getParameter("action");
        }

        switch (action) {
            case "home":
                out.append("<body> <h1>Startseite</h1>" +
                        "Dies ist die Startseite - yeah- </br> " +
                        "Jeremy Gondolf </br>" +
                        "701721 </body>");
                break;
            case "header":
                out.append("<body> <h1>HttpHeader</h1> </br");
                out.append("<table style = \"border =1px solid black\">");
                for (Enumeration<String> headerNames = request.getHeaderNames(); headerNames.hasMoreElements(); ) {
                    String headerName = headerNames.nextElement();
                    out.append("<tr>");
                    out.append("<td>" + "HeaderName:" + headerName + "  </td>");
                    for (Enumeration<String> headerValues = request.getHeaders(headerName); headerValues.hasMoreElements(); ) {
                        out.append("<td>");
                        out.append("HeaderValues: "+headerValues.nextElement() + " </br> ");
                        out.append("</td>");
                    }
                    out.append("</tr>");
                }
                out.append("</table>");
                out.append("</body>");
                break;
            case "cookies":
                Cookie[] cookies = request.getCookies();
                out.append("<body> <h1>Cookies</h1> </br");
                out.append("<table style = \"border =1px solid black\">");
                if (cookies != null) {
                    for (Cookie c : cookies) {
                        out.append("<tr> \n"
                                +"<td>" + "Cookie-Name: " + c.getName() + ";  </td> \n"
                                +"<td>" + "MaxAge: " + c.getMaxAge() + ";</td>\n"
                                +"</tr>");
                    }
                }
                out.append("</table>");
                LocalDateTime localDateTime = LocalDateTime.now();
                Cookie custumCookie = new Cookie("keks"+DateTimeFormatter.ofPattern("yyyMMddHHmmss").format(localDateTime),"" );
                custumCookie.setMaxAge(30);
                response.addCookie(custumCookie);
                break;
            case "search":
                String searchEngine = response.encodeURL("http://www.google.de");
                response.sendRedirect(searchEngine);
                break;

        }
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
        doGet(request, response);
    }

}
