package login;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * login.Login-Servlet
 * constructs a small formula, that requests a username
 * after submitting a session starts for the user
 * user gets logged out when the session end, gets deleted
 * count user
 */
@WebServlet(urlPatterns = {"/login"})
public class Login extends HttpServlet {

    public AtomicInteger userCounter = new AtomicInteger(0);


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = "";
        PrintWriter out = response.getWriter();
        out.append("<body>");
        if (request.getSession() == null || request.getSession().getAttribute("username") == null) {
            if (!(request.getParameter("username").equals("") || request.getParameter("username") == null)) {
                username = request.getParameter("username");
                HttpSession userSession = request.getSession(true);
                userSession.setAttribute("username", username);
                request.getServletContext().setAttribute("userCount", userCounter.incrementAndGet());
                response.sendRedirect(request.getContextPath() + "/chat");
            } else {
                //response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Missing Username!");
                //build formula
                out.append("<form id='login' method='POST'>");
                //label for username input
                out.append("<label for='username' form='login'>Username:</label>");
                //input field requesting a username
                out.append("<input type='text' name='username' maxlength='30'>");
                //submit button
                out.append("<button type='submit' value='Submit'>Submit</button>");
                out.append("<b>Please insert a username!</b>");
                //end formula
                out.append("</form>");
            }
        } else {
            userCounter.decrementAndGet();
            request.getSession().invalidate();
            response.sendRedirect(request.getContextPath() + "/login");
        }
        out.append("</body>");
        response.setContentType("text/html");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        //get Writer for output
        PrintWriter out = response.getWriter();
        out.append("<body>");

        if (request.getSession() == null || request.getSession().getAttribute("username") == null) {
            //build formula
            out.append("<form id='login' method='POST'>");
            //label for username input
            out.append("<label for='username' form='login'>Username:</label>");
            //input field requesting a username
            out.append("<input type='text' name='username' maxlength='30'>");
            //submit button
            out.append("<button type='submit' value='Submit'>Submit</button>");
            //end formula
            out.append("</form>");
        } else {
            //build formula
            out.append("<form id='logout' method='POST'>");
            out.append("<button type='submit' value='logout'>Logout</button>");
        }
        out.append("</body>");

        response.setContentType("text/html");

    }
}
