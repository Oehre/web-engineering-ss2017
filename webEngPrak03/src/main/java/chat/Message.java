package chat;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Message {
    private String message;
    private String user;
    private String timestamp;
    public Message(String m, String u) {
        this.message = m;
        this.user = u;
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        this.timestamp = sdf.format(new Date());
    }

    public String getMessage() {
        return this.message;
    }

    public String getUser() {
        return this.user;
    }

    public String getTimestamp() {
        return this.timestamp;
    }

    @Override
    public String toString() {
        return "User:"+getUser()+" Datum:"+getTimestamp()+" Nachricht:"+getMessage();
    }
}
