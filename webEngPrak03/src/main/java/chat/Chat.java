package chat;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * simple chat servlet
 */
@WebServlet(urlPatterns = {"/chat"})
public class Chat extends HttpServlet {
    public ConcurrentLinkedQueue<Message> queue;

    @Override
    public void init() throws ServletException {
        super.init();
        queue = new ConcurrentLinkedQueue<>();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        if (request.getParameter("send") != null) {
            if (request.getParameter("message") != null) {
                Message message = new Message("" + request.getParameter("message"), "" + request.getSession().getAttribute("username"));
                queue.add(message);
            }
            if (queue.size() > 10) {
                queue.poll();
            }
            response.sendRedirect(request.getContextPath() + "/chat");
        } else {
            RequestDispatcher dispatcher = request.getRequestDispatcher("login");
            dispatcher.forward(request, response);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        out.append("<body");
        RequestDispatcher dispatcher = request.getRequestDispatcher("login");
        dispatcher.include(request, response);
        out.append("</br>");
        if (request.getSession() != null && request.getSession().getAttribute("username") != null) {
            out.append("Username: " + request.getSession().getAttribute("username"));
            out.append("</br>");

            out.append("UserCount:" + request.getServletContext().getAttribute("userCount"));
            out.append("</br>");

        }
        out.append("<form id='chat' method = 'post'> ");
        //input field requesting a username
        out.append("<input type='text' name='message' >");
        //submit button
        out.append("<button type='submit' name = 'send' value='send'>Enter</button>");
        out.append("</form>");
        if (!queue.isEmpty()) {
            out.append("<table>");
            out.append("<tr>");
            out.append("<th>Message</th>");
            out.append("</tr>");
            for (Message message : queue) {
                out.append("<tr>");
                out.append("<td>" + message + "</td>");
                out.append("</tr>");
            }
            out.append("<table>");

        }


        out.append("</body");
    }
}
