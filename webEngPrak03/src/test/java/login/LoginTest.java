package login;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class LoginTest {

    @Mock
    HttpServletRequest request;

    @Mock
    HttpServletResponse response;

    @Mock
    HttpSession session;

    @Mock
    ServletContext ctx;

    StringWriter sw;

    @Before
    public void init() throws Exception {
        MockitoAnnotations.initMocks(this);
        when(request.getSession()).thenReturn(session);
        when(request.getServletContext()).thenReturn(ctx);
        sw = new StringWriter();
        when(response.getWriter()).thenReturn(new PrintWriter(sw));
    }

    @Test
    public void testLoginButton() throws ServletException, IOException {
        new Login().doGet(request, response);
        assertTrue("Expected login Button Got:" + sw.toString(), sw.toString().contains("<button type='submit' value='Submit'>Submit</button>"));
    }

    @Test
    public void testLogoutButton() throws ServletException, IOException {
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("username")).thenReturn("test");
        new Login().doGet(request, response);
        verify(session.getAttribute("username"));
        assertTrue("Expected logout Button Got:" + sw.toString(), sw.toString().contains("<button type='submit' value='logout'>Logout</button>"));
    }


}
